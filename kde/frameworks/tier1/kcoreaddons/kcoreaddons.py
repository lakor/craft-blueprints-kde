import info


class subinfo(info.infoclass):
    def setTargets(self):
        self.versionInfo.setDefaultValues()

        self.description = "KCoreAddons"

        self.patchToApply["5.90.0"] = [("0001-Draft-klibexec-helper-to-resolve-libexec-path-relati.patch", 1)]
        self.patchLevel["5.90.0"] = 1
        self.patchToApply["5.91.0"] = [("0002-revert-host-tools-version-check.patch", 1)]
        self.patchLevel["5.91.0"] = 1

    def setDependencies(self):
        self.buildDependencies["virtual/base"] = None
        self.buildDependencies["kde/frameworks/extra-cmake-modules"] = None
        self.runtimeDependencies["libs/qt5/qtbase"] = None
        self.runtimeDependencies["libs/shared-mime-info"] = None


from Package.CMakePackageBase import *


class Package(CMakePackageBase):
    def __init__(self):
        CMakePackageBase.__init__(self)
